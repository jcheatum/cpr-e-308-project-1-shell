CFLAGS = -c -O3 -g

.PHONY: all
all: main.o line.o joblist.o
	gcc $^ -lpthread -o shell

%.o: src/%.c
	gcc $(CFLAGS) $<

# I already have vscode set up to use lldb, so clang is used for debug instead 
# of gcc since it uses llvm. This should not effect regular builds.
.PHONY: debug
debug:
	clang src/*.c -o shell -O0 -g

.PHONY: clean
clean:
	rm -f *.o

.PHONY: tarball
tarball:
	tar -cvf proj1-jcheatum.tar.gz src Makefile