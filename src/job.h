#ifndef _JOB_H
#define _JOB_H

#include <unistd.h>

/**
 *  Encapsulates the pid and name of a process so they can be passed to
 *  functions as a single argument.
 */
typedef struct {
    pid_t pid;
    char *name;
} Job;

#endif