#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/wait.h>
#include <pthread.h>

#include "line.h"
#include "joblist.h"

#define DEFAULT_PROMPT  "308sh> "
#define OPTIONS         ":p:"

JobList joblist;
pthread_mutex_t joblist_mutex;
Job *fg;
char *prompt;

/**
 *  Thread function that waits until a process is done, print the exit message,
 *  and remove it from the job list.
 */
void *watch_job(void *arg) {
    Job *j = (Job *) arg;
    int status = 0;
    waitpid(j->pid, &status, 0);
    if (WIFEXITED(status)) {
        printf("[pid %d] %s exited with status %d\n", j->pid, j->name, WEXITSTATUS(status));
    } else if (WIFSIGNALED(status)) {
        printf("[pid %d] %s exited with signal %d\n", j->pid, j->name, WTERMSIG(status));
    }

    // Lock mutex since the job list will be accessed from multiple threads.
    pthread_mutex_lock(&joblist_mutex);
    joblist_remove(&joblist, j);
    pthread_mutex_unlock(&joblist_mutex);

    return NULL;
}

/**
 *  Handle ctrl-c
 *
 *  If there is currently a process running in the foreground, kill it, 
 *  otherwise "kill" the current line (go to a new line as if the user had
 *  pressed enter on an empty line without executing the contents of the line).
 */
void INT_handler(int sig) {
    if (fg != NULL) {
        kill(fg->pid, sig);
        printf("\n");
    } else {
        printf("\n%s", prompt);
        fflush(stdout);
    }
    signal(SIGINT, INT_handler);
}

int main(int argc, char **argv) {
    /* Doing this instead of just assinging the string allows `prompt` to be 
     * resized to an arbitrary length if necessary. */
    prompt = malloc((strlen(DEFAULT_PROMPT) + 1) * sizeof(char));
    if (prompt == NULL) {
        perror("Error");
        exit(1);
    }
    strcpy(prompt, DEFAULT_PROMPT);

    /* Have ctrl-c kill the process running in the foreground instead of the 
     * shell */
    signal(SIGINT, INT_handler);

    // Parse command line options
    int opt;
    while ((opt = getopt(argc, argv, OPTIONS)) != -1) {
        switch (opt) {
        case 'p': // when -p PROMPT is passed, set the prompt
            // make sure the prompt string is long enough for the new prompt
            if (strlen(prompt) < strlen(optarg)) {
                prompt = realloc(prompt, (strlen(optarg) + 1) * sizeof(char));
            }
            strcpy(prompt, optarg);
            break;
        case ':': // Option is missing an argument
            fprintf(stderr, "Error: Option '-%c' requires an argument\n", optopt);
            exit(1);
        case '?': // Unknown option is passed
            fprintf(stderr, "Error: Unknown option: '-%c'\n", optopt);
            exit(1);
        }
    }

    joblist_init(&joblist);
    pthread_mutex_init(&joblist_mutex, NULL);
    int status = 0;
    while (1) {
        printf("%s", prompt);
        fflush(stdout);
        char *line = fgetline(stdin);

        // Do nothing if the line is empty
        if (strlen(line) == 0) {
            free(line);
            continue;
        }

        Tokenized t = tokenize_line(line);
        if (*t.tokv == NULL) {
            free(line);
            free(t.tokv);
            continue;
        }

        // Detect `&` operator
        int wait_options = 0;
        bool background = false;
        if (strcmp(t.tokv[t.tokc - 1], "&") == 0) {
            wait_options |= WNOHANG;
            background = true;
            t.tokv[t.tokc - 1] = NULL;
            t.tokc--;
        }

        // replace '~' with $HOME in paths
        int i, j;
        for (i = 0; t.tokv[i] != NULL; i++) {
            bool found_tilde = false;
            for (j = 0; j < strlen(t.tokv[i]); j++) {
                if (t.tokv[i][j] == '~' && (j == 0 || t.tokv[i][j-1] != '\\')) {
                    found_tilde = true;
                    break;
                }
            }

            if (found_tilde) {
                char *home = getenv("HOME");
                if (home == NULL) {
                    perror("Error");
                    continue;
                }
                char *path = malloc((strlen(t.tokv[i]) + strlen(home) + 1) * sizeof(char));
                if (path == NULL) {
                    perror("Error");
                    exit(1);
                }
                /* ensure the end of the string is a null byte so it doesn't
                 * read out of bounds */
                path[0] = '\0'; 

                // add anything that came before the tilde
                strncpy(path, t.tokv[i], j);

                path[j] = '\0';
                strcat(path, home);
                strcat(path, &t.tokv[i][j+1]);
                t.tokv[i] = path;
                t.need_free[i] = true;
            }
        }

        if (strcmp(t.tokv[0], "exit") == 0) {           // `exit` builtin
            joblist_remove_all(&joblist);
            for (i = 0; i < t.tokc; i++) {
                if (t.need_free[i]) {
                    free(t.tokv[i]);
                }
            }
            free(t.tokv);
            free(t.need_free);
            free(line);
            free(prompt);
            free(joblist.head);
            free(joblist.tail);
            exit(0);
        } else if (strcmp(t.tokv[0], "pid") == 0) {     // `pid` builtin
            printf("pid: %d\n", getpid());
        } else if (strcmp(t.tokv[0], "ppid") == 0) {    // `ppid` builtin
            printf("parent pid: %d\n", getppid());
        } else if (strcmp(t.tokv[0], "cd") == 0) {      // `cd` builtin
            char *path;
            if (t.tokv[1] == NULL) {
                path = getenv("HOME");
                if (path == NULL) {
                    perror("Error");
                    continue;
                }
            } else {
                path = t.tokv[1];
            }
            chdir(path);
        } else if (strcmp(t.tokv[0], "pwd") == 0) {     // `pwd` builtin
            char buf[PATH_MAX];
            getcwd(buf, PATH_MAX);
            printf("%s\n", buf);
        } else if (strcmp(t.tokv[0], "jobs") == 0) {    // `jobs` builtin
            pthread_mutex_lock(&joblist_mutex);
            joblist_print_jobs(&joblist);
            pthread_mutex_unlock(&joblist_mutex);
        } else {                                        // program commands
            int rc = fork();
            if (rc < 0) {
                perror("Error");
            } else if (rc == 0) {
                execvp(t.tokv[0], t.tokv);

                // Handle exec error
                perror("Error");
                exit(1); // Exit child process
            } else {
                printf("[pid %d] %s\n", rc, t.tokv[0]);

                /* `malloc()` new job (as opposed to just `Job j;`) and all of
                 * its pointer fields so that name and pid of background jobs 
                 * aren't overwritten with each new job. */
                Job *job = malloc(sizeof(Job));
                if (job == NULL) {
                    perror("Error");
                    exit(1);
                }
                job->name = malloc((strlen(t.tokv[0]) + 1) * sizeof(char));
                if (job->name == NULL) {
                    perror("Error");
                    exit(1);
                }
                strcpy(job->name, t.tokv[0]);
                job->pid = rc;

                pthread_mutex_lock(&joblist_mutex);
                joblist_add(&joblist, job);
                pthread_mutex_unlock(&joblist_mutex);

                /* Create a new thread to wait in the job in the background and
                 * print when it completes. This allows the shell to continue 
                 * running when the `&` operator is used. */
                pthread_t thread;
                pthread_create(&thread, NULL, watch_job, job);

                /* Prevent process completion message from printing on the same
                 * line as the next prompt when running in the foreground. */
                if (!background) {
                    fg = job;
                    pthread_join(thread, NULL);
                    fg = NULL;
                }

                waitpid(rc, &status, wait_options);
            }
        }

        /* free heap pointers that are allocated every
         * loop iteration to avoid memory leak */
         for (i = 0; i < t.tokc; i++) {
            if (t.need_free[i]) {
                free(t.tokv[i]);
            }
        }
        free(line);
        free(t.tokv);
        free(t.need_free);
    }
}