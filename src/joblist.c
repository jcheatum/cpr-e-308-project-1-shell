#include <stdio.h>
#include <stdlib.h>

#include "joblist.h"

void joblist_init(JobList *list) {
    struct node *head = malloc(sizeof(struct node));
    if (head == NULL) {
        perror("Error");
        exit(1);
    }

    struct node *tail = malloc(sizeof(struct node));
    if (tail == NULL) {
        perror("Error");
        exit(1);
    }

    head->next = tail;
    head->prev = NULL;
    head->job = NULL;

    tail-> next = NULL;
    tail->prev = head;
    tail->job = NULL;

    list->tail = tail;
    list->head = head;
    list->len = 0;
}

void joblist_add(JobList *list, Job *job) {
    struct node *n = malloc(sizeof(struct node));
    if(n == NULL) {
        perror("Error");
        exit(1);
    }

    n->next = list->tail;
    n->prev = list->tail->prev;
    n->job = job;

    list->tail->prev = n;
    n->prev->next = n;

    list->len++;
}

int joblist_remove(JobList *list, Job *job) {
    struct node *n = list->head->next;
    if (n == list->tail) return -1;

    while (n->job->pid != job->pid) {
        if (n->next == NULL) {
            perror("Error");
            exit(1);
        } else {
            n = n->next;
        }
    }

    n->prev->next = n->next;
    n->next->prev = n->prev;
    free(n->job->name);
    free(n->job);
    free(n);

    list->len--;

    return 0;
}

void joblist_remove_all(JobList *list) {
    while (list->head->next != list->tail) {
        joblist_remove(list, list->head->next->job);
    }
}

void joblist_print_jobs(JobList *list) {
    struct node *n = list->head->next;
    if (n == list->tail) return;

    for (; n->next != NULL; n = n->next) {
        printf("%d\t%s\n", n->job->pid, n->job->name);
    }
}