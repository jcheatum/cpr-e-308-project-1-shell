#ifndef _JOBLIST_H
#define _JOBLIST_H

#include "job.h"

struct node {
    struct node *prev;
    struct node *next;
    Job *job;
};

/**
 *  A doubly-linked list of jobs.
 */
typedef struct {
    struct node *head;
    struct node *tail;
    size_t len;
} JobList;

/**
 *  Initialize the JobList pointed to by `list`.
 */
void joblist_init(JobList *list);

/**
 *  Append `job` to `list`. 
 */
void joblist_add(JobList *list, Job *job);

/**
 *  Remove `job` from `list`. Returns 0 if successful, -1 on failure.
 */
int joblist_remove(JobList *list, Job *job);

/**
 *  Remove all jobs from `list`.
 */
void joblist_remove_all(JobList *list);

void joblist_print_jobs(JobList *list);

#endif