#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "line.h"

char *fgetline(FILE *stream) {
    char *buf = malloc(GET_BUF_W * sizeof(char));
    if (buf == NULL) {
        perror("Error");
        exit(1);
    }
    int len = GET_BUF_W, i = 0;
    char c = 0;

    /* Append characters to `buf` until the end of the line, resizing `buf` as
     * necessary. */
    while (c != '\n') {
        c = fgetc(stream);
        buf[i++] = c;
        if (i == len) {
            buf = realloc(buf, (len *= 2) * sizeof(char));
        }
    }

    // remove trailing newline and make it a proper string
    buf[i - 1] = '\0'; 
    return buf;
}

enum TokMode {
    NORM,
    SQUOTE,
    DQUOTE
};

Tokenized tokenize_line(char *line) {
    char **tokbuf = malloc(TOK_BUF_W * sizeof(char *));
    if (tokbuf == NULL) {
        perror("Error");
        exit(1);
    } 
    bool *need_free = malloc(TOK_BUF_W * sizeof(bool));
    char *quotebuf = NULL;
    enum TokMode mode = NORM;
    int len = TOK_BUF_W, tokidx = 0;
    char *token = strtok(line, " ");
    bool found_squote = false, found_dquote = false;
    do {
        // Set true if an odd number of quotes are found, false if even
        int i, j;
        for (i = 0; i < strlen(token); i++) {
            if (token[i] == '\'' && !found_dquote) {
                found_squote = !found_squote;
                // remove quote from the string
                for (j = i; j < strlen(token); j++) {
                    token[j] = token[j+1];
                }
            }
            if (token[i] == '"' && !found_squote) {
                found_dquote = !found_dquote;
                for (j = i; j < strlen(token); j++) {
                    token[j] = token[j+1];
                }
            }
        }

        /* Process tokens differently depending on whether or not we're in a
         * quote. */
        switch (mode) {
        case NORM:
            if (found_squote) {
                /* if token contains a single quote, initialize quotebuf,
                 * append token to quotebuf, and set mode to SQUOTE */
                mode = SQUOTE;

                quotebuf = malloc((strlen(token) + 2) * sizeof(char));
                if (quotebuf == NULL) {
                    perror("Error");
                    exit(1);
                }
                strcpy(quotebuf, token);
                strcat(quotebuf, " ");
                found_squote = false;
            } else if (found_dquote) {
                /* if token contains a double quote, initialize quotebuf,
                 * append token to quotebuf, and set mode to DQUOTE */
                mode = DQUOTE;

                quotebuf = malloc((strlen(token) + 2) * sizeof(char));
                if (quotebuf == NULL) {
                    perror("Error");
                    exit(1);
                }
                strcpy(quotebuf, token);
                strcat(quotebuf, " ");
                found_dquote = false;
            } else { // add normal space delimited tokens to tokbuf
                need_free[tokidx] = false;
                tokbuf[tokidx++] = token;
            }
            break;

        case SQUOTE:
            /* Tokens between single quotes are appended to quotebuf to create
             * a single longer token */
            quotebuf = realloc(quotebuf, (strlen(quotebuf) + strlen(token) + 3) * sizeof(char));
            strcat(quotebuf, token);
            strcat(quotebuf, " "); // re-add the space removed by `strtok()`
            if (found_squote) {
                need_free[tokidx] = true;
                tokbuf[tokidx++] = quotebuf;
                quotebuf = NULL;
                mode = NORM;
                found_squote = false;
            }
            break;

        case DQUOTE:
            // same logic as squote but with double quotes
            quotebuf = realloc(quotebuf, (strlen(quotebuf) + strlen(token) + 3) * sizeof(char));
            strcat(quotebuf, token);
            strcat(quotebuf, " ");
            if (found_dquote) {
                need_free[tokidx] = true;
                tokbuf[tokidx++] = quotebuf;
                quotebuf = NULL;
                mode = NORM;
                found_dquote = false;
            }
            break;
        }
        token = strtok(NULL, " ");
        if (tokidx == len) {
            tokbuf = realloc(tokbuf, (len *= 2) * sizeof(char *));
            need_free = realloc(need_free, len * sizeof(bool));
        }
    } while (token != NULL);

    if (quotebuf != NULL) {
        free(quotebuf);
    }
    
    /* `execvp()` requires that the array passed as the argv for the new 
     * process end with a null pointer */
    tokbuf[tokidx] = NULL;
    
    Tokenized t;
    t.tokc = tokidx;
    t.tokv = tokbuf;
    t.need_free = need_free;
    return t;
}